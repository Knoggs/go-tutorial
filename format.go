package main

import "fmt"

func main() {
	number := 45
	fmt.Printf("%T ", number)
	fmt.Printf("%v ", number)
	var x string = fmt.Sprintf("%T ", number)
	fmt.Printf("X ", x)
	fmt.Printf("Number: %x", 3435)
} 