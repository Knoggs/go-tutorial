package main

import (
	"fmt"
	"bufio"
	"os"
	"strconv"
)

func main() {
	//fmt.Println("Please enter some text: ")
	scanner := bufio.NewScanner(os.Stdin) // prompt for input
	fmt.Printf("Please enter your year of birth: ")
	scanner.Scan() // wait until enter is pressed
	input,_ := strconv.ParseInt(scanner.Text(), 10, 64) // capture input
	fmt.Printf("You typed: %d \n", input) // output the captured input
	fmt.Printf("You will be %d years old at end of the year \n", 2022 - input) 

}
